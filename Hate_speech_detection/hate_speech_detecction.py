# -*- coding: utf-8 -*-
"""
Created on Tue Sep 13 20:59:05 2022

@author: Siddharth
"""

import os
import pandas as pd
import numpy as np
import re
from sklearn.utils import resample
from sklearn.pipeline import Pipeline
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.linear_model import SGDClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import f1_score
import argparse

# Preprocessing text 

def clean_text(df, text_field):
    df[text_field] = df[text_field].str.lower()
    df[text_field] = df[text_field].apply(lambda x: re.sub(r"(@[A-Za-z0-9]+)|([^0-9A-Za-z \t])|(\w+:\/\/\S+)|^rt|http.+?", "", x))
    return df

#%%
def parse_args():
    parser = argparse.ArgumentParser(description='TweetClassifier')
    parser.add_argument("--tweet", required=False,
                        help='Tweet text')
    args = parser.parse_args()
    return args

#%%
def load_model():
    # Loading and analyzing data
    # os.chdir("D:/Machine_learning_projects/NLP/Hate_speech_detection/data")
    train = pd.read_csv('train.csv')
    test = pd.read_csv('test.csv') 
    # print(train)
    
    test_clean = clean_text(test, 'tweet')
    train_clean = clean_text(train, 'tweet')

    
    #%%
    
    # Oversampling minority data to remove imbalances in dataset
    train_majority = train_clean[train_clean.label==0]
    train_minority = train_clean[train_clean.label==1]
    
    train_minority_upsampled = resample(train_minority,
                                        replace=True,
                                        n_samples=len(train_majority),
                                        )                                          
    train_upsampled = pd.concat([train_minority_upsampled, train_majority])
    train_upsampled['label'].value_counts()
    #%%
    
    # Creating the NLP pipeline
    tc_pipeline = Pipeline([
        ('vect', CountVectorizer()),
        ('tfidf', TfidfTransformer()),
        ('nb', SGDClassifier())])
    
    #%%
    # Training the model
    X_train, X_test, y_train, y_test = train_test_split(train_upsampled['tweet'],
                                                        train_upsampled['label'])
    model = tc_pipeline.fit(X_train, y_train)
    return model, X_test, y_test

#%%
# Testing the model
def main():
    args = parse_args()
    # print(bool(args.tweet))
    
    model, X_test, y_test = load_model()
    # print(type(X_test))
    if args.tweet:
        test_data = X_test[0:0]
        test_data['tweet'] = args.tweet
        
        y_pred = model.predict(test_data)
        if y_pred[0]:
            print("This is a hate tweet")
        else:
            print("This not a hate tweet")
    else:
        y_pred = model.predict(X_test)
        print(f'Model accuracy for inference data: {f1_score(y_test, y_pred)}')

if __name__=='__main__':
    main()
