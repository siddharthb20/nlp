import numpy as np
import argparse
from utils import check_input, structure_data, initialization
from eda import plot_categories, length, clean_text, split_data
from model import feature_engg, models
import pandas as pd


def parse_args():
    argparser = argparse.ArgumentParser(description="Program to classify text")

    argparser.add_argument('-i', '--input_dir', help='Input directory for the dataset')

    args = argparser.parse_args()
    return args

def main():
    args = parse_args()
    # initialization()
    input_ok = check_input(args.input_dir)
    df = structure_data(args.input_dir)
    # plot_categories(df)
    # length(df)

    print("Cleaning text")
        
    df, labels = clean_text(df)
    train_df, test_df, y_train, y_test = split_data(df, labels)

    X_train, X_test = feature_engg(train_df, test_df)
    models(X_train=X_train, y_train=y_train, X_test=X_test, y_test=y_test)

if __name__ == '__main__':
    main()
