from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.model_selection import RandomizedSearchCV, GridSearchCV
from sklearn.metrics import classification_report, confusion_matrix, accuracy_score

def feature_engg(X_train, X_test):
        
    ngram_range = (1, 2)
    min_df = 10
    max_df = 1.0
    max_features = 300

    tfidf = TfidfVectorizer(
        encoding='utf-8',
        ngram_range=ngram_range,
        stop_words=None,
        lowercase=False,
        max_df=max_df,
        min_df=min_df,
        max_features=max_features,
        norm='l2',
        sublinear_tf=True
    )

    features_train = tfidf.fit_transform(X_train['clean_data']).toarray()
    features_test = tfidf.fit_transform(X_test['clean_data']).toarray()

    print(features_train.shape)
    return features_train, features_test

def models(X_train, y_train, X_test, y_test):
    
    n_estimators = [200, 800]
    max_features = ['auto', 'sqrt']
    max_depth = [10, 40]
    max_depth.append(None)

    min_samples_split = [10, 70, 100]
    min_samples_leaf = [2, 4, 8, 16]
    learning_rate = [.1, .5]
    subsample = [.5, .6, .75]

    random_grid = {
        'n_estimators' : n_estimators,
        'max_features' : max_features,
        'max_depth' : max_depth,
        'min_samples_split' : min_samples_split,
        'min_samples_leaf' : min_samples_leaf,
        'learning_rate' : learning_rate,
        'subsample' : subsample
    }

    gbc = GradientBoostingClassifier()

    random_search = RandomizedSearchCV(
        estimator=gbc,
        param_distributions=random_grid,
        n_iter=50,
        scoring='accuracy',
        cv=3,
        verbose=1,
    )

    random_search.fit(X_train, y_train)
    best_model = random_search.best_estimator_

    best_model.fit(X_train, y_train)
    preds =  best_model.predict(X_test)

    train_acc = accuracy_score(y_train, best_model.predict(X_train))  
    test_acc = accuracy_score(y_test, preds)

    print("The best hyperparameters from Random Search are:")
    print(random_search.best_params_)
    print(f'Training accuracy: {train_acc}  Test_accuracy: {test_acc}')
    print(f'Classification report\n{classification_report(y_test, preds)}')

