import seaborn as sns
import re
from utils import lemmatize, remove_stopwords
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import train_test_split

def plot_categories(df):

    sns.set_theme(style='whitegrid', palette='pastel')
    sns.histplot(x=df['category'], stat='percent')   

def length(df):
    df['length'] = df['contents'].str.len()
    sns.histplot(x=df['length'], hue=df['category'], kde=True)

def clean_text(df):
    
    df['clean_data'] = df['contents'].apply(lambda x: x.replace('\n', ' ').lower())
    df['clean_data'] = df['clean_data'].apply(lambda x: re.sub(r"-|\'|@|[|]|\(|\)|%|'[a-z]", '', x))
    df['clean_data'] = df['clean_data'].apply(lambda x: re.sub(r"\?|!|:|;|\"|,|\.|#", ' ', x))
    df['clean_data'] = df['clean_data'].apply(lambda x: re.sub(' +', ' ', x))
    df['clean_data'] = df['clean_data'].apply(lambda x: remove_stopwords(x))
    df['clean_data'] = df['clean_data'].apply(lambda x: lemmatize(x))
    le = LabelEncoder()
    labels = le.fit_transform(df['category'])

    # df['char_found'] = df['clean_data'].str.find('%')
    # print(df['char_found'].value_counts())
    return df, labels

def split_data(df, labels, ratio=0.15):
    train_df, test_df, y_train, y_test = train_test_split(df, labels, test_size=ratio)
    return train_df, test_df, y_train, y_test