import os
from constants import FILE_FORMATS
import zipfile
import pandas as pd
from nltk.stem import WordNetLemmatizer
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords
import nltk

def check_input(input_file):

    if os.path.isdir(input_file):
        input_type = 'folder' 
        return True

    else:
        filename, input_type = os.path.splitext(input_file)

        if input_type in FILE_FORMATS:
            return True
        
        elif 'zip' in input_type:
            unzip = input('Zip file found. Unzip? (y/n): ')  
            if unzip.lower() not in ['y', 'n']:
                raise ValueError('Invalid input. Try again!')
            if unzip.lower() == 'y':
              with zipfile.ZipFile(input_file, 'r') as zip_ref:
                zip_ref.extractall(os.path.split(filename)[0])
            return True

        else:
            return False


def structure_data(data_dir):
    categories = os.listdir(data_dir)
    for file in categories:
        if not os.path.isdir(os.path.join(data_dir, file)):
            categories.remove(file)
    
    df = pd.DataFrame()
    filenames = []
    file_cat = []

    # Add filenames to dataframe
    for category in categories:
        current_cat = os.path.join(data_dir, category)
        filenames.extend(os.listdir(current_cat))
        num_articles = len(os.listdir(current_cat))
        file_cat.extend(num_articles * [category])
        
    df['filename'] = filenames
    df['category'] = file_cat
    df['contents'] = read_files(data_dir, df)
    df = df.drop('relative_path', axis=1)

    return df


def read_files(data_dir, df):

    data = []
    df['relative_path'] = df['category'].str.cat(df['filename'], sep=os.sep)

    for f in df.relative_path:
        f = os.path.join(data_dir, f)

        file = open(os.path.join(data_dir, f), 'r')
        data.append(file.read())
        text = file.read()
        # print(type(text))
        # if '@' in data[-1]:
        #     print(data[-1])
        #     file.close()
        #     return
        file.close()

    
    return data
    
def initialization():
    nltk.download()

def lemmatize(text):

    lemmatizer = WordNetLemmatizer()
    tokens = word_tokenize(text)
    sentence_lemma = [lemmatizer.lemmatize(word) for word in tokens]
    return " ".join(sentence_lemma)

def remove_stopwords(text):

    stop_words = set(stopwords.words('english'))
    words = nltk.word_tokenize(text)
    final_token_set = [word for word in words if word not in stop_words]
    return " ".join(final_token_set)
