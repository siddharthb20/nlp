# -*- coding: utf-8 -*-
"""
Created on Tue Jun 14 10:53:42 2022

@author: Siddharth
"""

import nltk
import spacy
from nltk.stem import PorterStemmer, SnowballStemmer

stemmer = PorterStemmer()

words = ["eating", "eats", "eat", "ate", "adjustable", "rafting", "ability", "meeting"]
for word in words:
    print(word, stemmer.stem(word))
#%%
nlp = spacy.load('en_core_web_sm')
doc = nlp("eating eats eat ate adjustable rafting ability meeting better")
for token in doc:
    print(token, token.lemma_)

#%%
# Attribute ruler is case sensitive...
ar = nlp.get_pipe('attribute_ruler')
ar.add([[{'TEXT':'Bro'}], [{'TEXT':'Bruh'}]], {"LEMMA": "Brother"})
doc = nlp("Bro, you wanna go? Bruh, nah. I'm good")
for token in doc:
    print(token.lemma_)
    
#%%
lst_words = ['running', 'painting', 'walking', 'dressing', 'likely', 'children', 'whom', 'good', 'ate', 'fishing']
for word in lst_words:
    print(f"{word} -> {stemmer.stem(word)}")

doc = nlp("running painting walking dressing likely children who good ate fishing")
for word in doc:
    print(f"{word}->{word.lemma_}")