# -*- coding: utf-8 -*-
"""
Created on Tue Feb 15 00:25:55 2022

@author: Siddharth
"""

import spacy
import os

#%%
# Testing document iterable
nlp = spacy.blank("en")
document = nlp("Mr. Michael James Ross is a lawyer with integrity, who practises law in N.Y. But he didn't go to law school. This means he's just a man with integrity. However, Harvey Specter did go to law school and he charges $1000/hr.")
for idx, token in enumerate(document):
    if idx == 0:
        print(idx, token)
span = document[:9]
print(span)
print(type(span))

#%%
# Playing around with NLP methods
print(document[1], document[1].is_alpha)
print(document[47], document[47].like_num)
print(type(document[0]), type(document[0].text))
print(document[46], document[46].is_currency)
#%%
# Creating a sentencizer pipeline
nlp.add_pipe('sentencizer')
# print(nlp.pipe_names)
document = nlp("Mr. Michael James Ross is lawyer with integrity. But he didn't go to law school. This means he's just a man with integrity. However, Harvey Specter did go to law school and he charges $1000/hr.")
for token in document.sents:
    print(token)
#%%
# Extract all email IDs from text document
# directory = os.chdir('D:/Machine_learning_projects/NLP')
with open('data.txt') as f:
    text = f.readlines()

# Merge text
text = ' '.join(text)
doc = nlp(text)
for token in doc:
    if token.like_email:
        print(token)
        
#%%
text='''
Look for data to help you address the question. Governments are good
sources because data from public research is often freely available. Good
places to start include http://www.data.gov/, and http://www.science.
gov/, and in the United Kingdom, http://data.gov.uk/.
Two of my favorite data sets are the General Social Survey at http://www3.norc.org/gss+website/, 
and the European Social Survey at http://www.europeansocialsurvey.org/.
'''

seek_urls = spacy.blank('en')
doc = seek_urls(text)
urls = [t.text for t in doc if t.like_url]
print(urls)

#%%
transactions = "Tony gave two $ to Peter, Bruce gave 500 € to Steve"

nlp = spacy.blank('en')
doc = nlp(transactions)
for idx, token in enumerate(doc):
    if token.like_num and doc[idx+1].is_currency:
        print(token.text, doc[idx+1].text)