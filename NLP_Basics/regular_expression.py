# -*- coding: utf-8 -*-
"""
Created on Wed Aug 10 10:24:53 2022

@author: Siddharth
"""

import re


chat1 = 'codebasics: you ask lot of questions 😠  1235678912, abc@xyz.com'
chat2 = 'codebasics: here it is: (123)-567-8912, abc@xyz.com'
chat3 = 'codebasics: yes, phone: 1235678912 email: abc@xyz.com'

pattern = '\(\d{3}\)-\d{3}-\d{4}|\d{10}'
email_pattern = '[a-z0-9A-Z_]*@[a-z0-9A-Z]*\.[a-zA-Z]*'
matches = re.findall(email_pattern, chat3)


chat1='codebasics: Hello, I am having an issue with my order # 412889912'
chat2='codebasics: I have a problem with my order number 412889912'
chat3='codebasics: My order 412889912 is having an issue, I was charged 300$ when online it says 280$'

pattern = 'order[^\d]*(\d*)'
matches = re.findall(pattern, chat1)

text='''
Born	Elon Reeve Musk
June 28, 1971 (age 50)
Pretoria, Transvaal, South Africa
Citizenship	
South Africa (1971–present)
Canada (1971–present)
United States (2002–present)
Education	University of Pennsylvania (BS, BA)
Title	
Founder, CEO and Chief Engineer of SpaceX
CEO and product architect of Tesla, Inc.
Founder of The Boring Company and X.com (now part of PayPal)
Co-founder of Neuralink, OpenAI, and Zip2
Spouse(s)	
Justine Wilson
​
​(m. 2000; div. 2008)​
Talulah Riley
​
​(m. 2010; div. 2012)​
​
​(m. 2013; div. 2016)
'''

def extract_key_info(text):
    
    key_data = {}
    birthname_pattern = 'Born(.*)'
    age_pattern = 'age (\d+)'
    edu_pattern = 'Education(.*)'
    birthplace_pattern = 'age.*\n(.*)'

    key_data['name'] = re.findall(birthname_pattern, text)[0].strip()
    key_data['age'] = int(re.findall(age_pattern, text)[0])
    key_data['education'] = re.findall(edu_pattern, text)[0].strip()
    key_data['place of birth'] = re.findall(birthplace_pattern, text)[0].strip()
    
    return key_data

keydata = extract_key_info(text)
#print(keydata)

text = '''
Follow our leader Elon musk on twitter here: https://twitter.com/elonmusk, more information 
on Tesla's products can be found at https://www.tesla.com/. Also here are leading influencers 
for tesla related news,
https://twitter.com/teslarati
https://twitter.com/dummy_tesla
https://twitter.com/dummy_2_tesla
'''
pattern = 'twitter.com/([a-zA-Z0-9_]*)'

print(re.findall(pattern, text))

text = '''
Concentration of Risk: Credit Risk
Financial instruments that potentially subject us to a concentration of credit risk consist of cash, cash equivalents, marketable securities,
restricted cash, accounts receivable, convertible note hedges, and interest rate swaps. Our cash balances are primarily invested in money market funds
or on deposit at high credit quality financial institutions in the U.S. These deposits are typically in excess of insured limits. As of September 30, 2021
and December 31, 2020, no entity represented 10% or more of our total accounts receivable balance. The risk of concentration for our convertible note
hedges and interest rate swaps is mitigated by transacting with several highly-rated multinational banks.
Concentration of Risk: Supply Risk
We are dependent on our suppliers, including single source suppliers, and the inability of these suppliers to deliver necessary components of our
products in a timely manner at prices, quality levels and volumes acceptable to us, or our inability to efficiently manage these components from these
suppliers, could have a material adverse effect on our business, prospects, financial condition and operating results.
'''
pattern = 'Concentration of Risk: (.*)'

print(re.findall(pattern, text))

text = '''
Tesla's gross cost of operating lease vehicles in FY2021 Q1 was $4.85 billion.
BMW's gross cost of operating vehicles in FY2021 S1 was $8 billion.
'''

pattern = 'FY(\d{4} (?:Q[1-4]|S[1-2]))'
matches = re.findall(pattern, text)
print(matches)