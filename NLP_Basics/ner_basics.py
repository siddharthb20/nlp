# -*- coding: utf-8 -*-
"""
Created on Fri Jun 17 11:15:47 2022

@author: Siddharth
"""

import spacy
from spacy.tokens import Span
nlp = spacy.load('en_core_web_sm')

print(nlp.pipe_names)
#%%
doc = nlp("Tesla Inc is going to acquire Twitter for $45billion")

for ent in doc.ents:
    print(ent.text, ent.label_, spacy.explain(ent.label_))

#%%

s1 = Span(doc, 0, 1, label="ORG")
s2 = Span(doc, 6, 7, label="ORG")

doc.set_ents([s1, s2], default='unmodified')
for idx, ent in enumerate(doc.ents):
    print(ent.text, ent.label_)
#%%
text = """Kiran want to know the famous foods in each state of India. So, he opened Google and search for this question. Google showed that
in Delhi it is Chaat, in Gujarat it is Dal Dhokli, in Tamilnadu it is Pongal, in Andhrapradesh it is Biryani, in Assam it is Papaya Khar,
in Bihar it is Litti Chowkha and so on for all other states"""

doc = nlp(text)

gpes = [g for g in doc.ents if g.label_ == 'GPE']
print("Geographical location Names:", gpes)
#%%
text = """Sachin Tendulkar was born on 24 April 1973, Virat Kholi was born on 5 November 1988, Dhoni was born on 7 July 1981
and finally Ricky ponting was born on 19 December 1974."""

doc = nlp(text)

dobs = [d for d in doc.ents if d.label_ == 'DATE']
print("All Dates of birth:", dobs)
print("\nCount: ", len(dobs))