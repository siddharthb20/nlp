# -*- coding: utf-8 -*-
"""
Created on Mon Feb 14 22:21:27 2022

@author: Siddharth
"""

import spacy
nlp = spacy.load('en_core_web_sm')
doc = nlp("Mr. Rick Sorkin is the best lawyer in N.Y. Hands down!")

for sen in doc.sents:
    print(sen)
    # for word in sen:
    #     print(word)

#%%
import nltk
nltk.download()
#%%
from nltk.tokenize import sent_tokenize

input_str_1 = "Dr. Strange is a fraud. Harvey R. Specter isnt."
input_str_2 = "Mr. Mike Ross is a fraud. Dr. Specter isn't"
print(sent_tokenize(input_str_1))
print(sent_tokenize(input_str_2))