# -*- coding: utf-8 -*-
"""
Created on Sat Jun 18 13:37:47 2022

@author: Siddharth
"""

from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.model_selection import train_test_split
from sklearn.pipeline import Pipeline
from sklearn.metrics import classification_report
from sklearn.naive_bayes import MultinomialNB
import pandas as pd

#%%
corpus = [
    "Thor eating pizza, Loki is eating pizza, Ironman ate pizza already",
    "Apple is announcing new iphone tomorrow",
    "Tesla is announcing new model-3 tomorrow",
    "Google is announcing new pixel-6 tomorrow",
    "Microsoft is announcing new surface tomorrow",
    "Amazon is announcing new eco-dot tomorrow",
    "I am eating biryani and you are eating grapes"
]

v = TfidfVectorizer()
v.fit(corpus)
transform_output = v.transform(corpus)
#%%
print(v.vocabulary_)

all_features = v.get_feature_names_out()
for word in all_features:
    idx = v.vocabulary_.get(word)
    print(f"Word: {word}  Score: {v.idf_[idx]}")

#%%
df = pd.read_csv('Ecommerce_data.csv')

# Check class imbalance
print(df.label.value_counts())

df['label_int'] = df.label.map({
    'Household': 0,
    'Books': 1,
    'Electronics': 2,
    'Clothing & Accessories': 3})

x_train, x_test, y_train, y_test = \
    train_test_split(df.Text, df.label_int, stratify=df.label_int)

#%%
clf = Pipeline([
    ('vectorizer', TfidfVectorizer()),
    ('nb', MultinomialNB())
    ])

clf.fit(x_train, y_train)
y_pred = clf.predict(x_test)
print(classification_report(y_test, y_pred))