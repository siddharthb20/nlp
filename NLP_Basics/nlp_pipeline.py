# -*- coding: utf-8 -*-
"""
Created on Wed Feb 16 00:30:24 2022

@author: Siddharth
"""

import spacy


# Adding a pretrained pipeline
nlp = spacy.load('en_core_web_sm')
print(nlp.pipeline)
#%%
doc = nlp("Mr. Michael James Ross is a lawyer with integrity, who practises law in N.Y. But he didn't go to law school. This means he's just a man with integrity. However, Harvey Specter did go to law school and he charges $1000/hr.")

for token in doc:
    print(token, "|", token.pos_, "|", token.lemma_)

#%%
doc = nlp("Tesla is going to acquire Twitter for $45billion")

for ent in doc.ents:
    print(ent.text, "|", ent.label_, "|", spacy.explain(ent.label_))

#%%
from spacy import displacy

print(displacy.render(doc, style='ent'))
#%%
nlp = spacy.blank('en')
source_nlp = spacy.load("en_core_web_sm")
nlp.add_pipe("ner", source=source_nlp)
print(nlp.pipe_names)

#%%
text = '''Ravi and Raju are the best friends from school days.They wanted to go for a world tour and 
visit famous cities like Paris, London, Dubai, Rome etc and also they called their another friend Mohan to take part of this world tour.
They started their journey from Hyderabad and spent next 3 months travelling all the wonderful cities in the world and cherish a happy moments!
'''

doc = nlp(text)
prop_nouns = []

for token in doc:
    if token.pos_ == 'PROPN':
        prop_nouns.append(token)

print(f'Found {len(prop_nouns)} proper nouns:{prop_nouns}')

#%%
text = '''The Top 5 companies in USA are Tesla, Walmart, Amazon, Microsoft, Google and the top 5 companies in 
India are Infosys, Reliance, HDFC Bank, Hindustan Unilever and Bharti Airtel'''

nlp = spacy.blank('en')
source = spacy.load('en_core_web_sm')
nlp.add_pipe('ner', source=source)
print(nlp.pipe_names)

doc = nlp(text)
companies = []

for ent in doc.ents:
    if ent.label_ == 'ORG':
        companies.append(ent)
        
print(f"Found {len(companies)} companies: {companies}")