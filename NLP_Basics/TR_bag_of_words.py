# -*- coding: utf-8 -*-
"""
Created on Fri Jun 17 18:03:21 2022

@author: Siddharth
"""

import spacy
import numpy as np
import pandas as pd
import os
from sklearn.model_selection import train_test_split
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.naive_bayes import MultinomialNB
from sklearn.metrics import classification_report
from sklearn.pipeline import Pipeline
from sklearn.ensemble import RandomForestClassifier
from sklearn.neighbors import KNeighborsClassifier

#%%
os.chdir('D:/Machine_learning_projects/NLP')
df = pd.read_csv('spam.csv')
print(df.Category.value_counts())

#%%
# Creating and training a model
df['Spam'] = df.Category.apply(lambda x:1 if x=='spam' else 0)

x_train, x_test, y_train, y_test = train_test_split(df.Message, df.Spam,\
                                                    test_size=0.2)

v = CountVectorizer()

x_train_cv = v.fit_transform(x_train.values)

model = MultinomialNB()
model.fit(x_train_cv, y_train)

#%%
# Testing the model
x_test_cv = v.transform(x_test)
y_pred = model.predict(x_test_cv)
print(classification_report(y_test, y_pred))

#%%
clf = Pipeline([
    ('vectorizer', CountVectorizer()),
    ('nb', MultinomialNB())])
clf.fit(x_train, y_train)
y_pred = clf.predict(x_test)
print(classification_report(y_test, y_pred))

#%%
# Testing on IMDB dataset with Random Forest
df = pd.read_csv('movies_sentiment_data.csv')
df['num_sentiment'] = df.sentiment.apply(lambda x:1 if x=='positive' else 0)

clf = Pipeline([
    ('vectorizer', CountVectorizer()),
    ('rf', RandomForestClassifier(n_estimators=50, criterion='entropy'))])

x_train, x_test, y_train, y_test = train_test_split(df.review, df.num_sentiment)

clf.fit(x_train, y_train)
y_pred = clf.predict(x_test)
print("Random Forest result:")
print(classification_report(y_test, y_pred))

#%%
# Using KNN
clf = Pipeline([
    ('vectorizer', CountVectorizer()),
    ('knn', KNeighborsClassifier(n_neighbors=10, metric='euclidean'))])

x_train, x_test, y_train, y_test = train_test_split(df.review, df.num_sentiment)

clf.fit(x_train, y_train)
y_pred = clf.predict(x_test)
print("KNN result:")
print(classification_report(y_test, y_pred))

#%%
# Bayes
df = pd.read_csv('movies_sentiment_data.csv')
df['num_sentiment'] = df.sentiment.apply(lambda x:1 if x=='positive' else 0)

clf = Pipeline([
    ('vectorizer', CountVectorizer()),
    ('nb', MultinomialNB())])

x_train, x_test, y_train, y_test = train_test_split(df.review, df.num_sentiment)

clf.fit(x_train, y_train)
y_pred = clf.predict(x_test)
print("Naive Bayes result:")
print(classification_report(y_test, y_pred))