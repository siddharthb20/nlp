# -*- coding: utf-8 -*-
"""
Created on Sat Jun 18 12:57:40 2022

@author: Siddharth
"""

from sklearn.feature_extraction.text import CountVectorizer
import spacy
import pandas as pd
#%%

v = CountVectorizer(ngram_range=(1, 2))
v.fit(['Mike Ross and his wife moved to Seattle'])
print(v.vocabulary_)

#%%
corpus = [
    "Mike told a lie about his education",
    "Harvey hired Mike",
    "Harvey is lying about Mike's education"]

nlp = spacy.load('en_core_web_sm')
#%%

def pre_process_text(text):
    filtered_token = []
    doc = nlp(text)
    for token in doc:
        if token.is_stop or token.is_punct:
            continue
        filtered_token.append(token.lemma_)
    return " ".join(filtered_token)

corpus_processed = [pre_process_text(text) for text in corpus]
print(corpus_processed)
#%%
v = CountVectorizer(ngram_range=(1,2))
v.fit(corpus_processed)
print(v.vocabulary_)

#%%
df = pd.read_json('news_dataset.json')
print(df.category.value_counts())
min_samples = 1381
df_business = df[df.category=='BUSINESS'].sample(min_samples, random_state=2022)
df_sports = df[df.category=='SPORTS'].sample(min_samples, random_state=2022)
df_crime = df[df.category=='CRIME'].sample(min_samples, random_state=2022)
df_science = df[df.category=='SCIENCE'].sample(min_samples, random_state=2022)

df_balanced = pd.concat([df_business, df_sports, df_crime, df_science], axis=0)
print(df_balanced.category.value_counts())
#%%
from sklearn.model_selection import train_test_split


target = {'BUSINESS':0, 'SPORTS':1, 'CRIME':2, 'SCIENCE':3}
df_balanced['category_num'] = df_balanced.category.map(target)

X_train, X_test, y_train, y_test = \
    train_test_split(df_balanced.text, df_balanced.category_num, test_size=0.2, stratify=df_balanced.category_num)
    
#%%
print(X_train.shape)
print(X_train.head())
#%%
from sklearn.naive_bayes import MultinomialNB
from sklearn.pipeline import Pipeline
from sklearn.metrics import classification_report

clf = Pipeline([
    ('vectorizer', CountVectorizer(ngram_range=(1,2))),
    ('nb', MultinomialNB())])

clf.fit(X_train, y_train)
y_pred = clf.predict(X_test)
print(classification_report(y_test, y_pred))

#%%
df_balanced['preprocessed_text'] = df_balanced.text.apply(pre_process_text)

#%%
X_train, X_test, y_train, y_test = \
    train_test_split(df_balanced.preprocessed_text, df_balanced.category_num, test_size=0.2, stratify=df_balanced.category_num)
    
clf = Pipeline([
    ('vectorizer', CountVectorizer(ngram_range=(1,2))),
    ('nb', MultinomialNB())])

clf.fit(X_train, y_train)
y_pred = clf.predict(X_test)
print(classification_report(y_test, y_pred))