# -*- coding: utf-8 -*-
"""
Created on Fri Jun 17 23:09:40 2022

@author: Siddharth
"""

import spacy
from spacy.lang.en.stop_words import STOP_WORDS
import pandas as pd

#%%
print(STOP_WORDS)
#%%
nlp = spacy.load('en_core_web_sm')
doc = nlp("We just opened our wings, the flying part is coming soon")

def preprocess(text):
    
    doc = nlp(text)
    
    no_stop_words = [token.text for token in doc if not token.is_stop]
    return " ".join(no_stop_words)
#%%
print(preprocess("We just opened our wings, the flying part is coming soon"))

#%%
df = pd.read_json("doj_press.json", lines=True)
df = df[df['topics'].str.len()!=0]
df['contents_wo_sw'] = df['contents'].apply(preprocess)
print(df.contents_wo_sw)

#%%
text = '''
Thor: Love and Thunder is a 2022 American superhero film based on Marvel Comics featuring the character Thor, produced by Marvel Studios and 
distributed by Walt Disney Studios Motion Pictures. It is the sequel to Thor: Ragnarok (2017) and the 29th film in the Marvel Cinematic Universe (MCU).
The film is directed by Taika Waititi, who co-wrote the script with Jennifer Kaytin Robinson, and stars Chris Hemsworth as Thor alongside Christian Bale, Tessa Thompson,
Jaimie Alexander, Waititi, Russell Crowe, and Natalie Portman. In the film, Thor attempts to find inner peace, but must return to action and recruit Valkyrie (Thompson),
Korg (Waititi), and Jane Foster (Portman)—who is now the Mighty Thor—to stop Gorr the God Butcher (Bale) from eliminating all gods.
'''

doc = nlp(text)
stopwords = [t for t in doc if t.is_stop]
print("Number of stop words:", len(stopwords))
print("% of stop words:", len(stopwords)/len(doc)*100)

#%%
nlp.vocab['not'].is_stop = False

text1 = 'this is a good movie'
text2 = 'this is not a good movie'

print(preprocess(text1))
print(preprocess(text2))
#%%
text = ''' The India men's national cricket team, also known as Team India or the Men in Blue, represents India in men's international cricket.
It is governed by the Board of Control for Cricket in India (BCCI), and is a Full Member of the International Cricket Council (ICC) with Test,
One Day International (ODI) and Twenty20 International (T20I) status. Cricket was introduced to India by British sailors in the 18th century, and the 
first cricket club was established in 1792. India's national cricket team played its first Test match on 25 June 1932 at Lord's, becoming the sixth team to be
granted test cricket status.
'''

nlp = spacy.load('en_core_web_sm')
doc = nlp(text)

no_stop_doc = [t.text for t in doc if not (t.is_stop or t.is_punct)]
no_stop_word_doc = ' '.join(no_stop_doc)
word_count = {}
for word in no_stop_doc:
    if word!=' ' or word!='\n':
        if word not in word_count:
            word_count[word] = 1
        else:
            word_count[word] += 1

max_frq_word = max(word_count.keys(), key=(lambda key:word_count[key]))
print(max_frq_word)