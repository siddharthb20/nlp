# -*- coding: utf-8 -*-
"""
Created on Tue Jun 14 11:21:42 2022

@author: Siddharth
"""

import spacy
import os

#%%

nlp = spacy.load('en_core_web_sm')
doc = nlp("Harvey pushed Mike. Samantha was angry. She slapped Harvey. Faye was appalled.")

# for token in doc:
#     print(token, "|", token.pos_, spacy.explain(token.pos_),
#           " | ", token.tag_, spacy.explain(token.tag_))

# print(type(doc[1].pos_), doc[1].pos_)

# Trying to remove punctuation marks
tbr = []
txt = ''
for idx, token in enumerate(doc):
    if token.pos_ != 'PUNCT':
        txt = txt + token.text
        txt = txt + ' '

print(txt)

print(doc.count_by(spacy.attrs.POS))
#%%
os.chdir('D:/Machine_learning_projects/NLP')
#%%

with open('news_data.txt', 'r') as f:
    data = f.read()

nlp = spacy.load('en_core_web_sm')
doc = nlp(data)

nouns, num_pos = [], []
for token in doc:
    if token.pos_ == 'NOUN':
        nouns.append(token.text)
    elif token.pos_ == 'NUM':
        num_pos.append(token.text)
print(nouns)
print(num_pos)

pos_tags = doc.count_by(spacy.attrs.POS)
for k, v in pos_tags.items():
    print(doc.vocab[k].text, v)

